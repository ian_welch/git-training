.. Git Training documentation master file, created by
   sphinx-quickstart on Fri Nov 10 07:04:07 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Git Training
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   ssh-setup
   first-steps
   first-repo
   branching
   stashing
   remote-repos
   git-svn
   gitignore
   altering-history
   advanced
   tips
   todo-list

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
