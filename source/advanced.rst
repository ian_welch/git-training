Advanced Git Operations
=======================

Cherry Picking
--------------

.. todo:: Discuss cherry picking commits.

Aliases
-------

.. todo:: Discuss alais configuration.

Diff Tool and Merge Tool Configuration
--------------------------------------

.. todo:: Show how to configure git to use an alternate merge tool.

Splitting Directories out of a Repository
-----------------------------------------

.. todo:: Show how to split a repo in to multiple repos using :command:`git
          subtree split`.

Working with multiple Repositories
----------------------------------

Google Repo
^^^^^^^^^^^

* https://gerrit.googlesource.com/git-repo/

.. todo:: Discussion of using google repo.

Git Multi
^^^^^^^^^

The :command:`git-multi` is an enhancement to |git| which allows you to run a
|git| command on multiple repositories at the same time. Particularly useful
for me are the following::

    $ git multi status
    $ git multi grep 'some-stuff'

My implementation of :command:`git-multi` knows how to use Google Repo manifest
files to determine which repos to operate on:

    https://github.com/openavr-org/git-openavr
