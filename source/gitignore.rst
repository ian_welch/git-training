Git Ignore Files
================

Git has support for ignoring files via its :file:`.gitignore` files.

This is extremely useful since it is usually desirable to avoid checking in
files that are generated during a build. The :file:`gitignore` files will also
reduce the noise when you run :command:`git status`.

Git's :file:`.gitignore` files are similar to CVS's :file:`.cvsignore`
files and Subversion's :command:`svn:ignore` property. One major
difference between them is a :file:`.gitignore` will affect all
subdirectories whereas CVS and Subversion only ignore files in the
current directory. For example, if you put the following in the
:file:`.gitignore` file at the top level of your project::

  *.o
  *.bak
  tmp

then any file or directory within any directory of the repository that
matches those patterns will be ignored by git.

Git will also exclude files which match patterns defined in
:file:`<repo>/.git/info/exclude`.

For more detailed information on :file:`.gitignore` files::

    $ git help ignore
