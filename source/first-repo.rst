Your First Git Repo
===================

Before you can do anything useful with git, you will need a repository
to work with. Let's create an empty directory which will become your
first repository::

    [1]$ cd ~
    [2]$ mkdir -p repos/example
    [3]$ cd repos/example

Creating a repo with git is as simple as this::

    [4]$ git init

To see what your empty repo looks like::

    [5]$ find .

Create a new file in your repo::

    [6]$ cat > hello <<EOF
    #!/bin/bash
    echo "Hello world!"
    EOF
    [7]$ chmod 755 hello
    [8]$ ./hello

Get a status of the repo::

    [9]$ git status

Tell git you want to track the file::

    [10]$ git add hello
    [11]$ git status

And finally, commit the staged file to the repo::

    [12]$ git commit
    [13]$ git log
    [14]$ git status

Let's make some changes to :file:`hello`::

    [15]$ echo 'echo "goodbye"' >> hello
    [16]$ git status
    [17]$ git diff
    [18]$ git add hello
    [19]$ git status
    [20]$ git diff
    [21]$ git diff --cached

We now have some staged changes. Let's make another change before we
commit::

    [22]$ echo 'exit 0' >> hello
    [23]$ git status
    [24]$ git diff
    [25]$ git diff --cached

At this point, we can do one of two things:

* Commit the staged changes, then add the unstaged changes and commit
  again. This will give us two separate commits::

    [26]$ git commit
    [27]$ git add hello
    [28]$ git diff --cached
    [29]$ git commit
    [30]$ git log

* Stage the second change and commit. This will combine the two
  changes into a single change which will be a single commit::

    [31]$ git add hello
    [32]$ git diff --cached
    [33]$ git commit
    [34]$ git log

Try both of these methods (make a different change the seconds time around) and
compare the results.

At this point, you should have a basic understanding of the
:command:`git add`, :command:`git status`, :command:`git diff` and
:command:`git commit` commands.
