.. _intro-label:

Introduction
============

Git Training.

This training is meant to be a hands on, exercise based introduction
to |git|_. |git| is a source code management tool that is widely used in
the IT industry.

This document will *not* cover the following:

* Using |git| on Windows (although command line |git| via bash works).

Objectives
----------

* Create an empty local git repo.
* Make changes within your local repo.
* Working with branches in your local repo.
* Clone a remote repo.
* Publish your changes to a remote repo.
* Fetch/Merge/Pull changes from a remote repo.
* Collaborate with other developers.

The Pro Git Book
----------------

Much of the information for this document came directly from the |ProGitBook|_.

Reading this book from cover to cover and understanding it all will put you
well on the way to becoming an expert git user.

Before going any further with this document, you are strongly encouraged to
read the first three chapters of the book. If you do that, you may not need
this document, but it is hoped that this document will be a good companion to
the book.

Contributing to the Document
----------------------------

This is an open source document with the source project hosted on |gitlab|_

    https://gitlab.com/OpenAVR/git-training

Feel free to use the issue tracker there to report problems or to request
improvements to the document.

If you want to help improve the document, please fork it and send merge
requests.

The rendered HTML at http://openavr.gitlab.io/git-training/ is automatically
updated when changes are pushed to the master branch of the source project.

LICENSE
-------

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
Commons, PO Box 1866, Mountain View, CA 94042, USA.

.. image:: img/cc-by-sa-88x31.png
   :alt: Creative Commons License
   :align: center

.. _prereqs-label:

Prerequisites
-------------

There are a few things you need to do to prepare for the training.

* Install git tools on your system.

  * :command:`git`
  * :command:`gitk`
  * a diff tool (:command:`kdiff3`, :command:`meld`, :command:`xxdiff`, etc)
  * others???

* Create a user account on |gitlab|_, |github|_, or |bitbucket|_ (or any git
  hosting service you want to use; your company might host a service internally
  that you can use to collaborate with your co-workers).
